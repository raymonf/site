import re
import io
import jinja2
import json
import time
import secrets
import asyncio
import PyRSS2Gen
import sys
from aiohttp import web
from markupsafe import Markup
from datetime import datetime
import dateutil.parser
from functools import lru_cache
from markupsafe import Markup
import asyncio
import lxml
import struct
import binascii
from sqlalchemy import func

from db import Session, User, GroupChat, GroupChatMembership
from util import hash
from util.auth import AuthService
from util.misc import gen_uuid
import settings

def create_app(*, serve_static = False):
	app = App()
	
	app.router.add_get('/', page_index)
	app.router.add_get('/stats', page_stats)
	app.router.add_get('/forgot', page_forgot)
	app.router.add_post('/forgot', page_forgot)
	app.router.add_get('/reset/{token}', page_reset)
	app.router.add_post('/reset/{token}', page_reset)
	app.router.add_get('/Config/MsgrConfig.asmx', page_msgr_config)
	app.router.add_post('/Config/MsgrConfig.asmx', page_msgr_config)
	app.router.add_get('/config/MsgrConfig.asmx', page_msgr_config)
	app.router.add_post('/config/MsgrConfig.asmx', page_msgr_config)
	app.router.add_get('/etc/MsgrConfig', page_msgr_config)
	app.router.add_post('/etc/MsgrConfig', page_msgr_config)
	app.router.add_get('/etc/tabs', page_tabs)
	app.router.add_get('/etc/text-ad-service', page_textad)
	app.router.add_get('/etc/yahoo-chat-pane', page_yahoo_chat_pane)
	app.router.add_get('/etc/today-tab', lambda req: page_news(req, msntoday = True))
	app.router.add_get('/etc/today-msn', lambda req: page_news(req, msntoday = True, msn = True))
	app.router.add_get('/etc/today-wlm', lambda req: page_news(req, msntoday = True, wlm = True))
	app.router.add_get('/news', page_news)
	app.router.add_get('/news/rss', rss_news)
	app.router.add_get('/status', page_status)
	app.router.add_get('/faq', page_faq)
	app.router.add_get('/thanks', page_thanks)
	app.router.add_get('/downloads', page_downloads)
	app.router.add_get('/downloads/msn-messenger', page_downloads_msn)
	app.router.add_get('/downloads/yahoo-messenger', page_downloads_yahoo)
	app.router.add_get('/downloads/switcher', page_switcher)
	app.router.add_get('/login', page_login)
	app.router.add_post('/login', page_login)
	app.router.add_get('/groupchats', page_groupchats)
	app.router.add_get('/groupchats/{chat_id}/members', page_groupchat_members)
	app.router.add_post('/groupchats/{chat_id}/members', page_groupchat_members)
	app.router.add_get('/groupchats/{chat_id}/remove/{uuid}', page_groupchat_remove_member)
	app.router.add_post('/groupchats/{chat_id}/remove/{uuid}', page_groupchat_remove_member)
	app.router.add_get('/groupchats/{chat_id}/revoke/{uuid}', action_groupchat_revoke_invite)
	app.router.add_get('/groupchats/{chat_id}/leave', page_groupchat_leave)
	app.router.add_post('/groupchats/{chat_id}/leave', page_groupchat_leave)
	app.router.add_get('/groupchats/acceptInvite/{chat_id}', page_accept_groupchat_invite)
	app.router.add_get('/groupchats/declineInvite/{chat_id}', page_decline_groupchat_invite)
	app.router.add_get('/logout', action_logout)
	app.router.add_get('/account', page_account)
	app.router.add_post('/account', page_account)
	app.router.add_get('/account/change-password', page_change_password)
	app.router.add_post('/account/change-password', page_change_password)
	app.router.add_get('/account/puid', page_account_puid)
	app.router.add_get('/register', page_register)
	app.router.add_post('/register', page_register)
	app.router.add_get('/account-created', page_account_created)
	app.router.add_get('/connecting', page_connecting)
	app.router.add_get('/frontends', page_frontends)
	app.router.add_get('/get-started', page_get_started)
	if serve_static:
		app.router.add_static('/static', 'static')
	app.router.add_route('*', '/{path:.*}', handle_404)
	app.jinja_env = jinja2.Environment(
		loader = jinja2.FileSystemLoader('tmpl'),
		autoescape = jinja2.select_autoescape(default = True),
	)
	return app

# TODO: Remove all this server linking junk when we get a proper distributed server setup in place (that means not needing to improvise the database changes on the front-end due to the main server updating it in intervals, etc.)
class S2S(asyncio.Protocol):
	def __init__(self):
		self.transport = None
		self._buffer = b''
		self._writebuf = io.BytesIO()
		self.linked = False
		# TODO: Find a way to drop ditched replies?
		self._cmds_by_ts_by_cmd = {}
	
	def connection_made(self, transport):
		self.transport = transport
		self.send_reply('LINKSRV', settings.SITE_LINK_PASSWORD)
	
	def connection_lost(self, exc):
		self._buffer = b''
		self.transport = None
		self.linked = False
		
		if exc is None:
			print('Main server connection closed')
	
	def data_received(self, data):
		if self._buffer:
			self._buffer += data
		else:
			self._buffer = data
		
		for m in self._read():
			if not m[0].isnumeric():
				f = getattr(self, '_m_{}'.format(m[0].lower()))
				f(*m[1:])
			else:
				if len(m) > 1:
					detail = m[-1].split(' ')
					if len(detail) < 2: return
					self._add_cmd_by_ts(detail[0], detail[1], m)
	
	def send_reply(self, *m):
		self.write(m)
		if self.transport:
			self.transport.write(self.flush())
	
	# Page action functions
	
	def check_if_users_in_chat(self, chat_id, uuids):
		tses = []
		if not self.linked: return None
		
		for uuid in uuids:
			ts = self._gen_ts()
			tses.append(ts)
			self.send_reply('GRPCHAT', ts, chat_id, 'INCHAT', uuid)
		
		return tses
	
	def accept_groupchat_invite(self, chat_id, uuid):
		if not self.linked: return None
		
		ts = self._gen_ts()
		self.send_reply('GRPCHAT', ts, chat_id, 'ACCEPT', uuid)
		
		return ts
	
	def decline_groupchat_invite(self, chat_id, uuid):
		if not self.linked: return None
		
		ts = self._gen_ts()
		self.send_reply('GRPCHAT', ts, chat_id, 'DECLINE', uuid)
		
		return ts
	
	def revoke_groupchat_invite(self, chat_id, uuid):
		if not self.linked: return None
		
		ts = self._gen_ts()
		self.send_reply('GRPCHAT', ts, chat_id, 'REVOKE', uuid)
		
		return ts
	
	def send_groupchat_modify_role(self, chat_id, uuid, role, user_self):
		if not self.linked: return None
		
		ts = self._gen_ts()
		m = ('GRPCHAT', ts, chat_id, 'ROLE', uuid, role)
		if user_self is not None:
			# user_self not being None means we're transferring ownership and setting previous owner (user_self) to member
			m += (user_self.uuid,)
		self.send_reply(*m)
		
		return ts
	
	def remove_groupchat_member(self, chat_id, uuid):
		if not self.linked: return None
		
		ts = self._gen_ts()
		self.send_reply('GRPCHAT', ts, chat_id, 'REMOVE', uuid)
		
		return ts
	
	# Page action response stuff
	
	def _add_cmd_by_ts(self, cmd_name, ts, cmd):
		if cmd_name not in self._cmds_by_ts_by_cmd:
			self._cmds_by_ts_by_cmd[cmd_name] = {}
		self._cmds_by_ts_by_cmd[cmd_name][ts] = cmd
	
	def get_cmd_by_ts(self, cmd_name, ts):
		if cmd_name not in self._cmds_by_ts_by_cmd: return None
		cmd = self._cmds_by_ts_by_cmd[cmd_name].get(ts)
		if ts in self._cmds_by_ts_by_cmd[cmd_name]:
			del self._cmds_by_ts_by_cmd[cmd_name][ts]
		return cmd
	
	# Command handlers (for non-numeric responses)
	
	def _m_linksrv(self, op):
		if op == 'SUCCESS':
			self.linked = True
	
	def _m_grpchat(self, ts, cmd, *args):
		# Only use this to handle `INCHAT` messages for now; the rest of the responses are numerics which the listener handles
		if cmd == 'INCHAT':
			m = ['GRPCHAT', ts, cmd, *args]
			self._add_cmd_by_ts(m[0], ts, m)
	
	def _m_ping(self, challenge):
		self.send_reply('PONG', ':{}'.format(challenge))
	
	# Internal stuff
	
	def _gen_ts(self):
		return F'{time.time()}{gen_uuid()}'
	
	def write(self, m):
		self._writebuf.write(' '.join(map(str, m)).encode('utf-8'))
		self._writebuf.write(b'\r\n')
	
	def flush(self):
		data = self._writebuf.getvalue()
		if data:
			self._writebuf = io.BytesIO()
		return data
	
	def _read(self):
		while self._buffer:
			m = self._parse()
			if m is None: break
			yield m
	
	def _parse(self):
		try:
			i = self._buffer.index(b'\r\n')
		except IndexError:
			return None
		except ValueError:
			return None
		chunk = self._buffer[:i].decode('utf-8')
		self._buffer = self._buffer[i+2:]
		
		toks = []
		while True:
			chunk = chunk.lstrip(' ')
			if chunk[:1] == ':':
				toks.append(chunk[1:])
				break
			k = chunk.find(' ')
			if k < 0:
				tok = chunk
			else:
				tok = chunk[:k]
				chunk = chunk[k:]
			if tok:
				toks.append(tok)
			if k < 0:
				break
		return toks

class App(web.Application):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.stats = None
		self.server_status = None
		self.auth_service = AuthService()
		self.server_link = None
	
	async def startup(self):
		self.loop.create_task(self.link_site())
		self.loop.create_task(self.sync_service_status())
		self.loop.create_task(self.sync_stats())
	
	async def sync_service_status(self):
		while True:
			recv_time = 0
			send_time = 0
			difference_time = 0
			protocol = None
			status = None
			
			try:
				_, protocol = await self.loop.create_connection(MSNPTest, 'localhost', 1863)
				send_time = protocol.send_time
				
				while protocol.buffer.find(b'\r\n') == -1 and not protocol.transport.is_closing():
					recv_time = time.time()
					difference_time = recv_time - protocol.send_time
					if difference_time >= 10: break
					await asyncio.sleep(0.1)
				if protocol.transport.is_closing():
					status = 'down'
				else:
					if difference_time >= 10:
						status = 'slow'
					else:
						status = 'ok'
					protocol.transport.close()
			except asyncio.CancelledError as ex:
				raise ex
			except:
				if protocol is None:
					status = 'down'
			
			self.server_status = {
				'status': status,
				'last_updated': datetime.utcnow(),
			}
			
			await asyncio.sleep(120)
	
	async def link_site(self):
		for i in range(5):
			try:
				_, protocol = await self.loop.create_connection(S2S, 'localhost', 4309)
				self.server_link = protocol
				break
			except asyncio.CancelledError as ex:
				raise ex
			except:
				print('Could not connect to main server; trying again in 1 second')
			await asyncio.sleep(1)
		
		if self.server_link:
			self.loop.create_task(self._wait_for_link())
			pass
		else:
			print('5 seconds allotted - failed to make connection with main server. Terminating.')
			sys.exit(-1)
	
	async def _wait_for_link(self):
		server_link = self.server_link
		for i in range(50):
			if server_link.linked:
				print('Site service linked to main server')
				break
			await asyncio.sleep(0.1)
		
		if not server_link.linked:
			print('Site service could not link to main server')
			server_link.transport.close()
	
	async def sync_stats(self):
		import asyncio
		from datetime import timedelta
		from itertools import groupby
		import stats
		
		while True:
			now = datetime.utcnow()
			five_minutes_ago = now - timedelta(minutes = 5)
			hour_cutoff = now.timestamp() // 3600 - 24
			with stats.Session() as sess:
				stat = sess.query(stats.CurrentStats).filter(
					stats.CurrentStats.key == 'logged_in',
					stats.CurrentStats.date_updated > five_minutes_ago
				).one_or_none()
				logged_in = getattr(stat, 'value', 0)
				clients_by_id = {
					row.id: row.data
					for row in sess.query(stats.DBClient).all()
				}
				by_hour = _combine_by_client([
					{
						'hour': hcs.hour,
						'hour_formatted': _format_hour(hcs.hour),
						'client_formatted': _format_client(clients_by_id.get(hcs.client_id)),
						'users_active': int(hcs.users_active),
						'messages_sent': hcs.messages_sent,
						'messages_received': hcs.messages_received,
					}
					for hcs in sess.query(stats.HourlyClientStats).filter(stats.HourlyClientStats.hour >= hour_cutoff).all()
				])
				by_hour = sorted(by_hour, key = lambda x: (-x['hour'], -x['users_active']))
				by_hour = [
					(hour_formatted, list(l))
					for hour_formatted, l in groupby(by_hour, lambda x: x['hour_formatted'])
				]
			self.stats = {
				'logged_in': logged_in,
				'by_hour': by_hour,
			}
			await asyncio.sleep(300)

class MSNPTest(asyncio.Protocol):
	def __init__(self):
		self.transport = None
		self.buffer = b''
		self.send_time = 0
	
	def connection_made(self, transport):
		import time
		
		self.transport = transport
		self.transport.write(b'VER 1 MSNP18\r\n')
		self.send_time = time.time()
	
	def connection_lost(self, exc):
		self.buffer = b''
		self.transport = None
	
	def data_received(self, data):
		if self.buffer:
			self.buffer += data
		else:
			self.buffer = data

def _combine_by_client(l):
	by_key = {}
	for item in l:
		key = (item['hour'], item['client_formatted'])
		if key not in by_key:
			by_key[key] = item
			continue
		for s in ['users_active', 'messages_sent', 'messages_received']:
			by_key[key][s] += item[s]
	return list(by_key.values())

def _format_hour(hour):
	from datetime import datetime
	dt = datetime.fromtimestamp(hour * 3600)
	return dt.strftime("%Y-%m-%d, %H:00 - %H:59 UTC")

def _format_client(client):
	if client is None:
		return "(Unknown)"
	p = client['program']
	v = client['version']
	if p == 'msn':
		p = p.upper()
		# Skip troll/invalid versions
		try:
			if client['via'] != 'webtv':
				if v.startswith('MSNP'):
					v = _guess_msn_version(int(v[4:]))
				elif not re.match(r'^([0-9]\.?)+$', v):
					return None
		except ValueError:
			return None
		# Rely on custom/MSNP version string for WebTV clients until we find a reliable way to detect WebTV client builds
		if client['via'] == 'webtv':
			v = 'WebTV Client ({})'.format(v)
	elif p == 'yahoo':
		p = p.title()
	else:
		p = p.upper()
	s = "{} {}".format(p, v)
	if client['via'] not in ('direct', 'webtv'):
		s += ", {}".format(client['via'].upper())
	return s

def _guess_msn_version(dialect):
	if dialect <= 2:
		return "1.?"
	if dialect <= 4:
		return "2.?"
	if dialect <= 5:
		return "3.?"
	if dialect <= 7:
		return "4.?"
	return "?/MSNP" + str(dialect)

async def page_stats(req):
	return render(req, 'stats.html', {
		'title': 'Stats',
		'stats': req.app.stats,
	})

async def page_status(req):
	return render(req, 'status.html', { 'server_status': req.app.server_status })

async def page_thanks(req):
	return render(req, 'thanks.html', { 'title': 'Special Thanks' })

#async def page_stats_apis(req):
#	tmpl = req.app.jinja_env.get_template('apis/apis.nav.html')
#	nav = tmpl.render(on_api_stats = True)
#	
#	return render(req, 'apis/apis.stats.html', {
#		'title': 'Stats - APIs',
#		'nav': Markup(nav),
#	})
#
#async def page_stats_api_def(req):
#	resp = None
#	
#	api_name = req.match_info['api']
#	tmpl = req.app.jinja_env.get_template('apis/apis.nav.html')
#	nav = tmpl.render()
#	
#	try:
#		resp = render(req, 'apis/defs/apis.def.stats.{}.html'.format(api_name), {
#			'title': 'Stats - APIs',
#			'nav': Markup(nav),
#		})
#	except:
#		resp = render(req, '404.html', { 'title': 'Page not found' }, status = 404)
#	
#	return resp

async def page_index(req):
	from_mg = False
	referer = req.headers.get('Referer')
	if referer:
		from_mg = (referer.startswith('https://messengergeek.wordpress.com') or referer.startswith('https://wink.messengergeek.com'))
	
	return render(req, 'index.html', {
		'from_mg': from_mg,
	})

async def action_logout(req):
	auth_service = req.app.auth_service
	response = None
	
	if COOKIE_NAME in req.cookies:
		if _verify_cookie(req):
			auth_service.pop_token(PURPOSE_SITE, req.cookies.get(COOKIE_NAME))
			
			response = web.Response(status = 302, headers = {
				'Location': '/login?logged_out=true',
			})
	
	if response is None:
		response = web.Response(status = 302, headers = {
			'Location': '/',
		})
	
	if COOKIE_NAME in req.cookies:
		response.set_cookie(COOKIE_NAME, '', path = '/', expires = 'Thu, 01 Jan 1970 00:00:00 GMT')
	
	return response

async def page_login(req):
	errors = {}
	user = None
	
	if _verify_cookie(req):
		return web.HTTPFound('/')
	
	if req.method == 'POST':
		data = await req.post()
		
		email = data.get('email') or ''
		password = data.get('password') or ''
		if email is None or password is None:
			return web.HTTPInternalServerError()
		
		_check_email(errors, email)
		
		with Session() as sess:
			if not errors:
				user = _get_user(email)
				if user is None:
					errors['account'] = "Email doesn't exist or password is incorrect."
			
			if not errors:
				errors = verify_password(user, password)
			
			if not errors:
				if not _verify_cookie(req):
					response = web.Response(status = 302, headers = {
						'Location': req.query.get('next') or '/account',
					})
					
					_login(req, response, user)
					
					return response
		
	return render(req, 'login.html', {
		'title': 'Log in',
		'logged_out': req.query.get('logged_out') or False,
		'on_login': True,
		'errors': errors,
	})

def _login(req, resp, user):
	auth_service = req.app.auth_service
	
	token = auth_service.create_token(PURPOSE_SITE, user.email, lifetime = 86400)
	expiry = auth_service.get_token_expiry(PURPOSE_SITE, token)
	
	resp.set_cookie(COOKIE_NAME, token, path = '/', expires = datetime.utcfromtimestamp(expiry).strftime('%a, %d %b %Y %H:%M:%S GMT'))

async def page_register(req):
	auth_service = req.app.auth_service
	errors = None
	email = None
	created_email = None
	support_oldmsn = None
	support_yahoo = None
	
	if _verify_cookie(req):
		return web.HTTPFound('/')
	
	if req.method == 'POST':
		data = await req.post()
		
		if not errors:
			email = data.get('email') or ''
			username = data.get('username') or ''
			pw1 = data.get('password1') or ''
			pw2 = data.get('password2') or ''
			support_oldmsn = (data.get('support_oldmsn') == 'true')
			support_yahoo = (data.get('support_yahoo') == 'true')
			errors = create_user(email, username, pw1, pw2, support_oldmsn, support_yahoo)
		
		if not errors:
			with Session() as sess:
				user = _get_user(email)
				
				token = auth_service.create_token(PURPOSE_REGISTRATION, email)
				expiry = auth_service.get_token_expiry(PURPOSE_REGISTRATION, token)
				
				response = web.Response(status = 302, headers = {
					'Location': '/account-created',
				})
				
				response.set_cookie(REGISTRATION_COOKIE_NAME, token, path = '/', expires = datetime.utcfromtimestamp(expiry).strftime('%a, %d %b %Y %H:%M:%S GMT'))
				
				_login(req, response, user)
				
				return response
	
	return render(req, 'register.html', {
		'title': 'Register an account',
		'errors': errors,
		'email': email,
		'support_oldmsn': support_oldmsn,
		'support_yahoo': support_yahoo,
	})

async def page_account_created(req):
	cookies = req.cookies
	auth_service = req.app.auth_service
	
	reg_cookie = cookies.get(REGISTRATION_COOKIE_NAME) or ''
	email = auth_service.pop_token(PURPOSE_REGISTRATION, reg_cookie)
	if not email:
		response = web.Response(status = 302, headers = {
			'Location': '/',
		})
		if REGISTRATION_COOKIE_NAME in cookies:
			response.set_cookie(REGISTRATION_COOKIE_NAME, '', path = '/', expires = 'Thu, 01 Jan 1970 00:00:00 GMT')
		return response
	
	return render(req, 'account-created.html', {
		'title': 'Account created!',
		'email': email,
	})

async def page_get_started(req):
	return render(req, 'get-started.html', { 'title': 'Getting Started' })

async def page_connecting(req):
	return render(req, 'connecting.html', {
		'title': 'Connecting',
		'mirrored': _get_mirrored(),
	})

async def page_frontends(req):
	return render(req, 'frontends.html', { 'title': 'Frontends' })

async def page_news(req, msntoday = False, msn = False, wlm = False):
	with open('json/news.json', 'rb') as f:
		news_json = json.loads(f.read())
		f.close()
	
	entries = []
	
	for date, items in news_json.items():
		tmpl = req.app.jinja_env.get_template('news.entry.item.html')
		items_markup = [tmpl.render(item = Markup(item)) for item in items]
		
		tmpl = req.app.jinja_env.get_template('news.entry.html')
		entries.append(tmpl.render(date = dateutil.parser.isoparse(date).strftime('%Y-%m-%d'), items = Markup('\n'.join(items_markup))))
	
	return render(req, 'news.html', {
		'title': 'News',
		'entries': Markup('\n'.join(entries)),
		'msntoday': msntoday,
		'msn': msn,
		'wlm': wlm,
	})

async def rss_news(req):
	with open('json/news.json', 'rb') as f:
		news_json = json.loads(f.read())
		f.close()
	
	rss = PyRSS2Gen.RSS2(
		title = "Escargot Today",
		link = "https://escargot.log1p.xyz/news",
		description = "The latest news from Escargot Land.",
		docs = "",
		
		lastBuildDate = datetime.utcnow(),
		
		items = [
			PyRSS2Gen.RSSItem(
				title = dateutil.parser.isoparse(date).strftime('%Y-%m-%d'),
				description = ''.join(['<li>{}</li>\n'.format(entry) for entry in entries]),
				pubDate = dateutil.parser.isoparse(date),
			) for date, entries in news_json.items()
		]
	)
	
	return web.Response(status = 200, content_type = 'text/xml', text = rss.to_xml(encoding = 'utf-8'))

async def page_account(req):
	auth_service = req.app.auth_service
	errors = {}
	
	if not _verify_cookie(req):
		return web.HTTPFound('/login?next=/account')
	
	if req.method == 'POST':
		data = await req.post()
		action = data.get('action')
		if action is None:
			return web.HTTPInternalServerError()
		
		if action == 'updateInsecureFrontends':
			email = auth_service.get_token(PURPOSE_SITE, req.cookies.get(COOKIE_NAME)) or ''
			
			with Session() as sess:
				user = _get_user(email)
				if user is None:
					return web.HTTPInternalServerError()
				
				password = data.get('password') or ''
				if not hash.hasher.verify(password, user.password):
					errors['frontend'] = "Password doesn't match one on your account"
				
				if not errors:
					support_oldmsn = (data.get('old_msn') == 'true')
					support_yahoo = (data.get('yahoo') == 'true')
					
					if support_oldmsn:
						_set_password_msn(user, password)
					else:
						user._front_data = {key: value for key, value in user._front_data.items() if key != 'msn'}
					
					if support_yahoo:
						_set_password_yahoo(user, password)
					else:
						user._front_data = {key: value for key, value in user._front_data.items() if key != 'ymsg'}
					
					sess.add(user)
					
					return web.HTTPFound('/account?frontends_saved=true')
	
	email = auth_service.get_token(PURPOSE_SITE, req.cookies[COOKIE_NAME]) or ''
	
	with Session() as sess:
		user = _get_user(email)
		if user is None:
			return
		display_name = user.name
		if display_name == email:
			display_name = None
		
		dp_path = await _get_user_dp_path(user)
		
		return render(req, 'account.html', {
			'title': 'Account',
			'on_account': True,
			'user': user,
			'display_name': display_name,
			'dp_path': dp_path,
			'frontend_saved': (req.query.get('frontends_saved') == 'true'),
			'errors': errors,
		})

async def page_change_password(req):
	auth_service = req.app.auth_service
	errors = {}
	
	if not _verify_cookie(req):
		return web.HTTPFound('/login?next=/account/change-password')
	
	if req.method == 'POST':
		data = await req.post()
		
		email = auth_service.get_token(PURPOSE_SITE, req.cookies[COOKIE_NAME]) or ''
		with Session() as sess:
			user = _get_user(email)
			password = data.get('oldpassword') or ''
			
			errors = verify_password(user, password)
			
			if not errors:
				password1 = data.get('password1') or ''
				password2 = data.get('password2') or ''
				
				errors = change_password(user.email, password1, password2)
				
				if not errors:
					return web.HTTPFound('/account/change-password?changed=true')
	
	return render(req, 'change-password.html', {
		'title': 'Change Password - Account',
		'on_account': True,
		'errors': errors,
		'changed': (req.query.get('changed') == 'true'),
	})
	
async def page_account_puid(req):
	auth_service = req.app.auth_service
	
	if not _verify_cookie(req):
		return web.HTTPFound('/login?next=/account/puid')
	
	email = auth_service.get_token(PURPOSE_SITE, req.cookies[COOKIE_NAME]) or ''
	with Session() as sess:
		user = _get_user(email)
		puid = _puid_format(user.uuid)
	
	return render(req, 'account-puid.html', {
		'title': 'PUID - Account',
		'on_account': True,
		'puid': puid,
	})

async def page_groupchats(req):
	auth_service = req.app.auth_service
	user = None
	accepted_groupchats = []
	
	if _verify_cookie(req):
		email = auth_service.get_token(PURPOSE_SITE, req.cookies[COOKIE_NAME]) or ''
		with Session() as sess:
			user = _get_user(email)
			accepted_groupchats, _ = _get_groupchat_by_user(user)
	
	return render(req, 'groupchat/groupchats.html', {
		'title': 'Group Chats',
		'accepted_groupchats': accepted_groupchats,
		'declined': (req.query.get('declined') == 'true'),
		'doesnotexist': (req.query.get('doesnotexist') == 'true'),
	})

async def page_accept_groupchat_invite(req):
	auth_service = req.app.auth_service
	server_link = req.app.server_link
	chat_id = req.match_info.get('chat_id', '')
	
	if not _verify_cookie(req):
		return web.HTTPFound('/login?next=/groupchats/acceptInvite/{}'.format(chat_id))
	
	with Session() as sess:
		email = auth_service.get_token(PURPOSE_SITE, req.cookies[COOKIE_NAME]) or ''
		user = _get_user(email)
		
		groupchat = _get_groupchat_by_chat_id(chat_id)
		if groupchat is None:
			return render(req, 'groupchat/invite-process-failed.html', { 'title': 'Could not process group chat invite' }, status = 500)
		
		accept_cmd = None
		if server_link:
			ts = server_link.accept_groupchat_invite(chat_id, user.uuid)
			accept_cmd = await _wait_for_response(server_link, 'GRPCHAT', ts)
		if accept_cmd is None:
			return render(req, 'main-server-unavailable.html', { 'title': 'Could not process group chat invite' }, status = 500)
		
		if accept_cmd[0] == '201':
			return render(req, 'groupchat/invite-accept-success.html', {
				'title': 'Invite accepted!',
				'name': groupchat.name,
			})
		elif accept_cmd[0] in ('203','204'):
			return render(req, 'groupchat/invite-process-failed.html', status = 500)
		
		return web.HTTPInternalServerError()

async def page_decline_groupchat_invite(req):
	auth_service = req.app.auth_service
	server_link = req.app.server_link
	chat_id = req.match_info.get('chat_id', '')
	
	if not _verify_cookie(req):
		return web.HTTPFound('/login?next=/groupchats/declineInvite/{}'.format(chat_id))
	
	with Session() as sess:
		email = auth_service.get_token(PURPOSE_SITE, req.cookies[COOKIE_NAME]) or ''
		user = _get_user(email)
		
		groupchat = _get_groupchat_by_chat_id(chat_id)
		if groupchat is None:
			return render(req, 'groupchat/invite-process-failed.html', { 'title': 'Could not process group chat invite' }, status = 500)
		
		decline_cmd = None
		
		if server_link:
			ts = server_link.decline_groupchat_invite(chat_id, user.uuid)
			decline_cmd = await _wait_for_response(server_link, 'GRPCHAT', ts)
		if decline_cmd is None:
			return render(req, 'main-server-unavailable.html', status = 500)
		
		if decline_cmd[0] == '201':
			await asyncio.sleep(1)
			return web.HTTPFound('/groupchats?declined=true')
		elif decline_cmd[0] in ('203','204'):
			return render(req, 'groupchat/invite-process-failed.html', { 'title': 'Could not process group chat invite' }, status = 500)
		
		return web.HTTPInternalServerError()

async def page_groupchat_members(req):
	auth_service = req.app.auth_service
	server_link = req.app.server_link
	chat_id = req.match_info.get('chat_id', '')
	
	if not _verify_cookie(req):
		return web.HTTPFound('/login?next=/groupchats/{}/members'.format(chat_id))
	
	membership_list = []
	owner_memberships = []
	errors = {}
	transfer_owner = False
	owner_transferred = False
	membership_other = None
	target_member_name = None
	target_member_uuid = None
	target_member_email = None
	old_role = None
	new_role = None
	new_role_int = None
	
	inchat_cmds = []
	
	# Incoming spaghetti code alert!!
	with Session() as sess:
		email = auth_service.get_token(PURPOSE_SITE, req.cookies[COOKIE_NAME]) or ''
		user = _get_user(email)
		
		groupchat = _get_groupchat_by_chat_id(chat_id)
		if groupchat is None:
			return web.HTTPFound('/groupchats?doesnotexist=true')
		
		membership = _get_groupchat_membership(groupchat, user)
		if membership is None or (membership['role'] == 0 or membership['state'] in (0,4)):
			return web.HTTPFound('/groupchats?doesnotexist=true')
		
		if req.method == 'POST':
			# If caller isn't an owner or admin, refuse their request
			if membership['role'] not in (1,2):
				return web.HTTPFound('/groupchats/{}/members'.format(groupchat.chat_id))
			
			body = await req.post()
			
			# Get necessary info to perform request
			target_member_uuid = body.get('uuid')
			new_role = body.get('role')
			confirm = True if body.get('confirm') == 'true' else False
			
			# Preliminary checks
			if None in (target_member_uuid,new_role):
				return web.HTTPInternalServerError()
			if new_role not in ('1','2','3'):
				return web.HTTPInternalServerError()
			
			groupchat_user = _get_user_by_uuid(target_member_uuid)
			if groupchat_user is None:
				errors['groupchat'] = "doesnt-exist"
			if not errors:
				membership_other = _get_groupchat_membership(groupchat, groupchat_user)
				if membership_other is None or (membership['role'] == 0 or membership['state'] in (0,4)):
					errors['groupchat'] = "doesnt-exist"
				if not errors:
					# Can't change your own role
					if membership_other['uuid'] == user.uuid:
						errors['groupchat'] = "You cannot change the role of yourself."
					if not errors:
						# If transferring ownership, check if the caller is the groupchat owner
						if new_role == '1' and membership['role'] != 1:
							errors['groupchat'] = "You do not have sufficient permissions to transfer ownership."
							if not errors:
								# Users have to confirm if they want to transfer ownership first to avoid any surprises
								if membership_other['role'] != 1:
									if confirm is not True:
										return render(req, 'groupchat/owner-transfer-confirmation.html', {
											'email': groupchat_user.email,
											'chat_id': groupchat.chat_id,
											'uuid': groupchat_user.uuid,
										})
									else:
										transfer_owner = True
						
						if not errors:
							# Go ahead and send S2S command to actually change role
							role_cmd = None
							old_role = membership_other['role']
							target_member_email = groupchat_user.email
							target_member_name = groupchat_user.friendly_name
							if server_link:
								ts_role = server_link.send_groupchat_modify_role(chat_id, groupchat_user.uuid, new_role, (user if transfer_owner else None))
								role_cmd = await _wait_for_response(server_link, 'GRPCHAT', ts_role)
							
							# Since this junk is actually reliant on the main server functioning (too much pain to consider a fallback
							# method with the amount of spaghetti code already put into this), error out if it's down.
							if role_cmd is None:
								errors['link'] = "The main server cannot be contacted to perform your action at the moment. Try again later."
							
							if not errors:
								# If command wasn't successful, check from a list of possible error values (check `core/s2s/ctrl.py` in main server for definitons on error codes)
								if role_cmd[0] != '201':
									if role_cmd[0] == '200':
										return web.HTTPFound('/groupchats?doesnotexist=true')
									elif role_cmd[0] == '203':
										errors['groupchat'] = "The member you tried to change the role of does not exist in the group chat."
									elif role_cmd[0] == '205':
										errors['groupchat'] = "The member you tried to change the role of is pending. Pending members roles can't have their roles changed until they've been accepted."
									elif role_cmd[0] == '206':
										return web.HTTPInternalServerError()
									else:
										# Or in the (rare but) possible event an unknown error occurs, report to user
										errors['link'] = "An unknown error has occurred while processing your request. Try again later."
								
								if not errors:
									new_role_int = int(new_role)
									# If we transferred ownership, set old owner's role to Member
									if transfer_owner:
										owner_transferred = True
										membership['role'] = 3
									# Refresh `GroupChat` object
									groupchat = _get_groupchat_by_chat_id(chat_id)
									# The groupchat not being available after our request is nigh impossible, but just in case...
									# XXX: We NEED to delegate Escargot's services into different parts to avoid this kind of nightmare!
									if groupchat is None:
										return web.HTTPFound('/groupchats?doesnotexist=true')
		
		# This part forges the updates into the `GroupChat` for the purpose of making sure it shows up almost instantly
		# to the end user since the main server STILL SYNCS CHANGES EVERY SECOND (doubt decreasing the interval will help much either)
		
		memberships = _get_all_groupchat_memberships(groupchat)
		
		members_membership_inchat_by_uuid = {}
		for membership1 in memberships:
			if (membership1['role'] == 0 or membership1['state'] in (0,4)): continue
			
			member_head = _get_user_by_uuid(membership1['uuid'])
			if member_head is None: continue
			members_membership_inchat_by_uuid[membership1['uuid']] = [member_head, membership1, False]
		if server_link:
			tses_inchat = server_link.check_if_users_in_chat(chat_id, list(members_membership_inchat_by_uuid.keys()))
		else:
			tses_inchat = None
		if tses_inchat is not None:
			inchat_cmds = [await _wait_for_response(server_link, 'GRPCHAT', ts_inchat) for ts_inchat in tses_inchat]
		
		if inchat_cmds:
			for m in inchat_cmds:
				if m[0] == 'GRPCHAT':
					uuid = m[3]
					members_membership_inchat_by_uuid[uuid][2] = (m[4].lower() == 'true')
		for member_head, membership1, in_chat in members_membership_inchat_by_uuid.values():
			ms = {
				'name': member_head.name, 'email': member_head.email, 'uuid': membership1['uuid'],
				'role': -1, 'state': membership1['state'],
				'invite_message': membership1['invite_message'], 'inviter_name': membership1['inviter_name'],
				'in_chat': in_chat,
			}
			
			if new_role_int is not None and membership1['uuid'] == target_member_uuid:
				ms['role'] = new_role_int
			if owner_transferred and membership_other['uuid'] == user.uuid:
				ms['role'] = 3
			if ms['role'] == -1:
				ms['role'] = membership1['role']
			membership_list.append(ms)
		
		return render(req, 'groupchat/members.html', {
			'title': 'Members in {}'.format(groupchat.name),
			'name': groupchat.name,
			'chat_id': groupchat.chat_id,
			'target_member_name': target_member_name,
			'target_member_email': target_member_email,
			'new_role': new_role_int,
			'errors': errors,
			"owner_transferred": owner_transferred,
			'owner_memberships': owner_memberships,
			'membership': membership,
			'membership_list': membership_list,
		})

async def page_groupchat_leave(req):
	auth_service = req.app.auth_service
	server_link = req.app.server_link
	chat_id = req.match_info.get('chat_id', '')
	
	if not _verify_cookie(req):
		return web.HTTPFound('/login?next=/groupchats/{}/leave'.format(chat_id))
	
	with Session() as sess:
		groupchat = _get_groupchat_by_chat_id(chat_id)
		if groupchat is None:
			return web.HTTPFound('/groupchats?doesnotexist=true')
		
		email = auth_service.get_token(PURPOSE_SITE, req.cookies[COOKIE_NAME]) or ''
		user = _get_user(email)
		
		membership = _get_groupchat_membership(groupchat, user)
		if membership is None or (membership['role'] == 0 or membership['state'] in (0,4)):
			return web.HTTPFound('/groupchats?doesnotexist=true')
		
		if membership['role'] == 1 and len(_get_groupchat_memberships_by_role(groupchat, 1)) < 2:
			return web.HTTPFound('/groupchats')
		
		if req.method == 'POST':
			remove_cmd = None
			
			if server_link:
				ts = server_link.remove_groupchat_member(chat_id, user.uuid)
				remove_cmd = await _wait_for_response(server_link, 'GRPCHAT', ts)
			if remove_cmd is None:
				return render(req, 'main-server-unavailable.html', status = 500)
			
			if remove_cmd[0] == '201':
				return render(req, 'groupchat/leave-success.html', {
					'title': 'You have left {}'.format(groupchat.name),
					'name': groupchat.name,
				})
			elif remove_cmd[0] in ('200','203'):
				return web.HTTPFound('/groupchats?doesnotexist=true')
			elif remove_cmd[0] == '207':
				return web.HTTPFound('/groupchats')
			else:
				return web.HTTPInternalServerError()
		elif req.method == 'GET':
			return render(req, 'groupchat/leave-confirmation.html', {
				'title': 'Leave group chat "{}"?'.format(groupchat.name),
				'name': groupchat.name,
				'chat_id': groupchat.chat_id,
			})

async def page_groupchat_remove_member(req):
	auth_service = req.app.auth_service
	server_link = req.app.server_link
	chat_id = req.match_info.get('chat_id', '')
	uuid = req.match_info.get('uuid', '')
	
	if not _verify_cookie(req):
		return web.HTTPFound('/login?next=/groupchats/{}/remove/{}'.format(chat_id, uuid))
	
	with Session() as sess:
		groupchat = _get_groupchat_by_chat_id(chat_id)
		if groupchat is None:
			return web.HTTPFound('/groupchats?doesnotexist=true')
		
		email = auth_service.get_token(PURPOSE_SITE, req.cookies[COOKIE_NAME]) or ''
		user = _get_user(email)
		
		membership = _get_groupchat_membership(groupchat, user)
		if membership is None or (membership['role'] == 0 or membership['state'] in (0,4)):
			return web.HTTPFound('/groupchats?doesnotexist=true')
		
		if membership['role'] not in (1,2):
			return web.HTTPUnauthorized()
		
		groupchat_user = _get_user_by_uuid(uuid)
		if groupchat_user is None:
			return web.HTTPInternalServerError()
		
		membership_other = _get_groupchat_membership(groupchat, groupchat_user)
		if membership_other is None or membership_other['role'] == 0 or membership_other['state'] in (0,4) or (membership_other['role'] == 1 and (len(_get_groupchat_memberships_by_role(groupchat, 1)) < 2) or membership['role'] == 2) or membership_other['uuid'] == user.uuid:
			return render(req, 'groupchat/couldnt-remove-member.html', {
				'title': 'Couldn\'t remove member {} from {}'.format(groupchat_user.email, groupchat.name),
				'email': groupchat_user.email,
				'chat_id': groupchat.chat_id,
			}, status = 500)
		
		if req.method == 'POST':
			remove_cmd = None
			if server_link:
				ts = server_link.remove_groupchat_member(chat_id, groupchat_user.uuid)
				remove_cmd = await _wait_for_response(server_link, 'GRPCHAT', ts)
			if remove_cmd is None:
				return render(req, 'main-server-unavailable.html', status = 500)
			
			if remove_cmd[0] == '201':
				return render(req, 'groupchat/member-remove-success.html', {
					'title': 'Member {} successfully removed!'.format(groupchat_user.email),
					'email': groupchat_user.email,
					'chat_id': groupchat.chat_id,
				})
			elif remove_cmd[0] == '200':
				return web.HTTPFound('/groupchats?doesnotexist=true')
			elif remove_cmd[0] == '203':
				return render(req, 'groupchat/couldnt-remove-member.html', {
					'title': 'Couldn\'t remove member {} from {}'.format(groupchat_user.email, groupchat.name),
					'email': groupchat_user.email,
					'chat_id': groupchat.chat_id,
				}, status = 500)
			elif remove_cmd[0] == '207':
				return web.HTTPFound('/groupchats')
			else:
				return web.HTTPInternalServerError()
		elif req.method == 'GET':
			return render(req, 'groupchat/member-remove-confirmation.html', {
				'title': 'Remove member {} from {}?'.format(groupchat_user.email, groupchat.name),
				'email': groupchat_user.email,
				'name': groupchat.name,
				'chat_id': groupchat.chat_id,
				'uuid': groupchat_user.uuid,
			})

async def action_groupchat_revoke_invite(req):
	auth_service = req.app.auth_service
	server_link = req.app.server_link
	chat_id = req.match_info.get('chat_id', '')
	uuid = req.match_info.get('uuid', '')
	
	if not _verify_cookie(req):
		return web.HTTPFound('/login?next=/groupchats/{}/revoke/{}'.format(chat_id, uuid))
	
	with Session() as sess:
		email = auth_service.get_token(PURPOSE_SITE, req.cookies[COOKIE_NAME]) or ''
		user = _get_user(email)
		
		groupchat = _get_groupchat_by_chat_id(chat_id)
		if groupchat is None:
			return web.HTTPFound('/groupchats?doesnotexist=true')
		
		membership = _get_groupchat_membership(groupchat, user)
		if membership is None or (membership['role'] == 0 or membership['state'] in (0,4)):
			return web.HTTPFound('/groupchats?doesnotexist=true')
		
		if membership['role'] not in (1,2):
			return web.HTTPUnauthorized()
		
		groupchat_user = _get_user_by_uuid(uuid)
		if groupchat_user is None:
			return web.HTTPInternalServerError()
		
		revoke_cmd = None
		if server_link:
			ts = server_link.revoke_groupchat_invite(chat_id, groupchat_user.uuid)
			revoke_cmd = await _wait_for_response(server_link, 'GRPCHAT', ts)
		if revoke_cmd is None:
			return render(req, 'main-server-unavailable.html', status = 500)
		
		if revoke_cmd[0] == '201':
			return render(req, 'groupchat/invite-revoke-success.html', {
				'title': 'Invite successfully revoked!',
				'email': groupchat_user.email,
				'chat_id': groupchat.chat_id,
			})
		elif revoke_cmd[0] == '200':
			return web.HTTPFound('/groupchats?doesnotexist=true')
		elif revoke_cmd[0] in ('203','204'):
			return render(req, 'groupchat/invite-revoke-failed.html', {
				'title': 'Could not revoke group chat invite',
				'email': groupchat_user.email,
				'chat_id': groupchat.chat_id,
			}, status = 500)
		
		return web.HTTPInternalServerError()

async def handle_404(req):
	return render(req, '404.html', { 'title': 'Page not found' }, status = 404)

async def page_yahoo_chat_pane(req):
	return render(req, 'yahoo-chat-pane.html')

async def page_tabs(req):
	with open('svcs_tabs.xml') as fh:
		tabs_resp = fh.read()
	with open('tabs.xml') as fh:
		config_tabs = fh.read()
	
	return web.HTTPOk(content_type = 'text/xml', text = tabs_resp.format(tabs = config_tabs))

async def page_textad(req):
	textad = ''
	# Use 'rb' to make UTF-8 text load properly
	with open('textads.json', 'rb') as f:
		textads = json.loads(f.read())
		f.close()
	
	if len(textads) > 0:
		if len(textads) > 1:
			ad = textads[secrets.randbelow((len(textads)-1))]
		else:
			ad = textads[0]
		with open('textad.xml') as fh:
			textad = fh.read()
		textad = textad.format(caption = ad['caption'], hiturl = ad['hiturl'])
	return web.HTTPOk(content_type = 'text/xml', text = textad)

async def page_msgr_config(req):
	if req.method == 'POST':
		body = await req.read()
	else:
		body = None
	msgr_config = _get_msgr_config(req, body)
	if msgr_config == 'INVALID_VER':
		return web.Response(status = 500)
	return web.HTTPOk(content_type = 'text/xml', text = msgr_config)

def _get_msgr_config(req, body):
	query = req.query
	result = None
	
	if query.get('ver') is not None:
		if re.match(r'[^\d\.]', query.get('ver')):
			return 'INVALID_VER'
		
		config_ver = query.get('ver').split('.', 4)
		if 8 <= int(config_ver[0]) <= 9:
			with open('MsgrConfig.wlm.8.xml') as fh:
				config = fh.read()
			with open('tabs.xml') as fh:
				config_tabs = fh.read()
			result = config.format(tabs = config_tabs)
		elif int(config_ver[0]) >= 14:
			with open('MsgrConfig.wlm.14.xml') as fh:
				config = fh.read()
			with open('tabs.xml') as fh:
				config_tabs = fh.read()
			result = config.format(tabs = config_tabs)
	elif body is not None:
		with open('MsgrConfig.msn.envelope.xml') as fh:
			envelope = fh.read()
		with open('MsgrConfig.msn.xml') as fh:
			config = fh.read()
		with open('tabs.xml') as fh:
			config_tabs = fh.read()
		result = envelope.format(MsgrConfig = config.format(tabs = config_tabs))
	
	return result or ''

async def page_forgot(req):
	errors = None
	email = None
	sent_to = None
	if req.method == 'POST':
		data = await req.post()
		email = data.get('email') or ''
		errors = send_password_reset(email, req.app.auth_service)
		if not errors:
			return web.HTTPFound('/forgot?sent_to={}'.format(email))
	else:
		sent_to = req.query.get('sent_to')
	
	return render(req, 'forgot.html', {
		'title': 'Forgot your password?',
		'errors': errors,
		'email': email,
		'sent_to': sent_to,
	})

async def page_reset(req):
	auth_service = req.app.auth_service
	
	token = req.match_info.get('token', '')
	email = auth_service.get_token(PURPOSE_PWRESET, token)
	valid_token = (email is not None)
	errors = None
	reset_success = False
	support_old = None
	
	if valid_token:
		if req.method == 'POST':
			data = await req.post()
			pw1 = data.get('password1') or ''
			pw2 = data.get('password2') or ''
			support_old = (data.get('support_old') == 'true')
			errors = change_password(email, pw1, pw2)
			if not errors:
				auth_service.pop_token(PURPOSE_PWRESET, token)
				reset_success = True
		else:
			with Session() as sess:
				user = _get_user(email)
				if user:
					support_old = user.get_front_data('msn', 'pw_md5')
	
	return render(req, 'reset.html', {
		'title': 'Reset password',
		'valid_token': valid_token,
		'errors': errors,
		'reset_success': reset_success,
		'support_old': support_old,
	})

async def page_faq(req):
	return render(req, 'faq.html', {
		'title': 'FAQ',
		'mirrored': _get_mirrored(),
	})

async def page_downloads(req):
	return render(req, 'downloads.html', { 'title': 'Downloads' })

@lru_cache()
def _get_downloads(client):
	import csv
	from itertools import groupby
	
	langs = {}
	with open('languages.csv') as fh:
		for row in csv.DictReader(fh):
			langs[row['code']] = row['name_english']
	
	with open('downloads.csv') as fh:
		rows = list(csv.DictReader(fh))
	
	mirrored = _get_mirrored()
	
	with open('display.csv') as fh:
		displays = list(csv.DictReader(fh))
	
	with open('flag_special.csv') as fh:
		special_flags = list(csv.DictReader(fh))
	
	displays_filtered = {d['version']: d for d in sorted(displays, key = lambda r: r['client']) if d['client'] == client}
	
	downloads = {}
	for c, clients in groupby(rows, key = lambda r: r['client']):
		if c == client:
			for langcode, lang_downloads in groupby(clients, key = lambda r: r['langcode']):
				lang_downloads = list(lang_downloads)
				lang_name = langs[langcode]
				if lang_name not in downloads:
					downloads[lang_name] = []
				flagcode = langcode
				for row in special_flags:
					if row['lang_name'] == flagcode:
						flagcode = row['flag_name']
				downloads[lang_name].extend((flagcode, lang_downloads))
			break
	return sorted(downloads.items(), key = lambda pair: pair[0]), mirrored, displays_filtered

def _get_mirrored():
	import csv
	
	with open('mirrored.csv') as fh:
		return { row['file'] for row in csv.DictReader(fh) }

async def page_downloads_msn(req):
	download, mirrored, display = _get_downloads('msn')
	
	return render(req, 'downloads.msn.html', {
		'title': 'Downloads for MSN Messenger',
		'download': download,
		'mirrored': mirrored,
		'display': display,
	})

async def page_downloads_yahoo(req):
	download, mirrored, display = _get_downloads('yahoo')
	
	return render(req, 'downloads.yahoo.html', {
		'title': 'Downloads for Yahoo! Messenger',
		'download': download,
		'mirrored': mirrored,
		'display': display,
	})

async def page_switcher(req):
	return render(req, 'switcher.html', {
		'title': 'Escargot Switcher',
		'mirrored': _get_mirrored(),
	})

def create_user(email, username, pw1, pw2, support_oldmsn, support_yahoo):
	errors = {}
	_check_email(errors, email)
	_check_username(errors, username)
	_check_passwords(errors, pw1, pw2)
	if errors: return errors
	
	with Session() as sess:
		user = _get_user(email)
		if user is not None:
			# "Email already in use."
			errors['email'] = "Email already in use."
			return errors
		user = _get_user_by_username(username)
		if user is not None:
			# "Username already in use."
			errors['username'] = "Username already in use."
			return errors
		
		if username.lower() == 'yahoohelper':
			# "Username restricted from use."
			errors['username'] = "Username restricted from use."
			return errors
		user = User(
			uuid = gen_uuid(), email = email, username = username, verified = False,
			friendly_name = email,
			settings = {}, groups = {},
		)
		_set_passwords(user, pw1, support_oldmsn = support_oldmsn, support_yahoo = support_yahoo)
		sess.add(user)
	
	return errors

def send_password_reset(email, auth_service):
	errors = {}
	_check_email(errors, email)
	if errors: return errors
	
	with Session() as sess:
		user = _get_user(email)
		if user is None:
			errors['email'] = Markup("Email not registered with Escargot. Did you <a href=\"/register\">sign up?</a>")
			return errors
		token = auth_service.create_token(PURPOSE_PWRESET, email, lifetime = 3600)
		sent = _send_password_reset_email(email, token)
		if not sent:
			auth_service.pop_token(PURPOSE_PWRESET, token)
			errors['email'] = 'Email could not be sent.'
	
	return errors

def verify_password(user, pw):
	errors = {}
	_check_passwords(errors, pw, pw)
	if errors: return errors
	
	verified = hash.hasher.verify(pw, user.password)
	if not verified:
		errors['password'] = "Email doesn't exist or password is incorrect."
	
	return errors

def change_password(email, pw1, pw2):
	errors = {}
	
	with Session() as sess:
		user = _get_user(email)
		_check_passwords(errors, pw1, pw2)
		if not errors:
			if hash.hasher.verify(pw1, user.password):
				errors['password2'] = "Password is already set on your account"
		if errors: return errors
		
		_set_passwords(user, pw1, support_oldmsn = 'msn' in user._front_data, support_yahoo = 'ymsg' in user._front_data)
		user.verified = True
		sess.add(user)
	
	return errors

async def _wait_for_response(server_link, cmd_name, ts):
	cmd = None
	
	for i in range(50):
		cmd = server_link.get_cmd_by_ts(cmd_name, ts)
		if cmd:
			break
		await asyncio.sleep(0.1)
	
	return cmd

async def _get_user_dp_path(user):
	with Session() as sess:
		dp_path = 'https://{host}/storage/usertile/{uuid}/static'.format(
			host = settings.DP_HOST, uuid = user.uuid,
		)
		
		import aiohttp
		try:
			async with aiohttp.TCPConnector(verify_ssl = False) as connector:
				async with aiohttp.ClientSession(connector = connector) as session:
					request = session.get(dp_path)
					async with request as resp:
						if resp.status != 200:
							dp_path = None
		except:
			dp_path = None
	
	return dp_path

def _uuid_to_high_low(u):
	import uuid
	u = uuid.UUID(u)
	high = u.time_low % (1<<32)
	low = u.node % (1<<32)
	return (high, low)

def _puid_format(u):
	import binascii, struct
	high, low = _uuid_to_high_low(u)
	n = (high * (2 ** 32)) + low
	return binascii.hexlify(struct.pack('>Q', n)).decode('utf-8').upper()

def _set_passwords(user, pw, *, support_oldmsn = False, support_yahoo = False):
	user.password = hash.hasher.encode(pw)
	if support_oldmsn:
		_set_password_msn(user, pw)
	if support_yahoo:
		_set_password_yahoo(user, pw)

def _set_password_msn(user, pw):
	pw_md5 = hash.hasher_md5.encode(pw)
	user.set_front_data('msn', 'pw_md5', pw_md5)

def _set_password_yahoo(user, pw):
	pw_md5_unsalted = hash.hasher_md5.encode(pw, salt = '')
	user.set_front_data('ymsg', 'pw_md5_unsalted', pw_md5_unsalted)
	
	pw_md5crypt = hash.hasher_md5crypt.encode(pw, salt = '$1$_2S43d5f')
	user.set_front_data('ymsg', 'pw_md5crypt', pw_md5crypt)

def _send_password_reset_email(email, token):
	if settings.DEBUG:
		print("""********
Password reset requested for {}.
Here is your token:

\t{}

********""".format(email, token))
		return True
	
	import sendgrid
	from sendgrid.helpers.mail import Mail, From
	
	message = Mail(
		from_email = From('no-reply@escargot.log1p.xyz', "Escargot"),
		to_emails = email,
		subject = "Escargot password reset requested",
		html_content = RESET_TEMPLATE.format(email = email, token = token),
	)
	try:
		sg = sendgrid.SendGridAPIClient(settings.SENDGRID_API_KEY)
		ret = sg.send(message)
	except:
		return False
	return 200 <= ret.status_code < 300

RESET_TEMPLATE = """
A password reset was requested for the Escargot (https://escargot.log1p.xyz) account associated with {email}.

To change your password, follow this link within the next 60 minutes:

https://escargot.log1p.xyz/reset/{token}

If you did not request a password reset, ignore this email.
""".strip()

def _get_user(email):
	with Session() as sess:
		return sess.query(User).filter(func.lower(User.email) == email.lower()).one_or_none()

def _get_user_by_uuid(uuid):
	with Session() as sess:
		return sess.query(User).filter(User.uuid == uuid).one_or_none()

def _get_user_by_username(username):
	with Session() as sess:
		return sess.query(User).filter(User.username == username).one_or_none()

def _get_groupchat_by_chat_id(chat_id):
	with Session() as sess:
		return sess.query(GroupChat).filter(GroupChat.chat_id == chat_id).one_or_none()

def _get_groupchat_by_user(user):
	accepted_groupchats = []
	pending_groupchats = []
	
	with Session() as sess:
		dbgroupchatmemberships = sess.query(GroupChatMembership).filter(GroupChatMembership.member_uuid == user.uuid).all()
		
		for dbgroupchatmembership in dbgroupchatmemberships:
			groupchat = sess.query(GroupChat).filter(GroupChat.chat_id == dbgroupchatmembership.chat_id).one_or_none()
			if groupchat is None: continue
			membership = _format_groupchat_membership(dbgroupchatmembership)
			if membership is not None:
				if not (membership['role'] == 0 or membership['state'] in (0,4)):
					if membership['role'] == 4 and membership['state'] == 1:
						pending_groupchats.append((groupchat.name, groupchat.chat_id, membership))
					else:
						accepted_groupchats.append((groupchat.name, groupchat.chat_id, membership, len([membership_other for membership_other in _get_all_groupchat_memberships(groupchat) if membership_other['role'] == 1])))
	
	return accepted_groupchats, pending_groupchats

def _get_groupchat_membership(groupchat, user):
	with Session() as sess:
		dbgroupchatmembership = sess.query(GroupChatMembership).filter(GroupChatMembership.chat_id == groupchat.chat_id, GroupChatMembership.member_uuid == user.uuid).one_or_none()
		if dbgroupchatmembership:
			return _format_groupchat_membership(dbgroupchatmembership)
		return None

def _get_all_groupchat_memberships(groupchat):
	with Session() as sess:
		dbgroupchatmemberships = sess.query(GroupChatMembership).filter(GroupChatMembership.chat_id == groupchat.chat_id)
		
		return [_format_groupchat_membership(dbgroupchatmembership) for dbgroupchatmembership in dbgroupchatmemberships]

def _format_groupchat_membership(dbgroupchatmembership):
	return {
		'uuid': dbgroupchatmembership.member_uuid,
		'role': dbgroupchatmembership.role, 'state': dbgroupchatmembership.state, 'blocking': dbgroupchatmembership.blocking,
		'inviter_email': dbgroupchatmembership.inviter_email, 'inviter_name': dbgroupchatmembership.inviter_name, 'invite_message': dbgroupchatmembership.invite_message,
	}

def _get_groupchat_memberships_by_role(groupchat, role):
	with Session() as sess:
		dbgroupchatmemberships = sess.query(GroupChatMembership).filter(GroupChatMembership.chat_id == groupchat.chat_id, GroupChatMembership.role == role)
		return [_format_groupchat_membership(dbgroupchatmembership) for dbgroupchatmembership in dbgroupchatmemberships]

def _verify_cookie(req):
	auth_service = req.app.auth_service
	
	return auth_service.get_token(PURPOSE_SITE, req.cookies.get(COOKIE_NAME) or '') is not None

def _role_to_str(role):
	if role == 0:
		label = "Empty"
	elif role == 1:
		label = "Owner"
	elif role == 2:
		label = "Administrator"
	elif role == 3:
		label = "Member"
	elif role == 4:
		label = "Pending"
	else:
		label = "Undefined"
	
	return label

def _check_email(errors, email):
	if not (6 <= len(email) < 60) or ('@' not in email):
		errors['email'] = "Invalid email."

def _check_username(errors, username):
	if not (4 <= len(username) <= 32) or not re.match(r'[A-Za-z0-9_\.]+$', username):
		errors['username'] = "Invalid username."
		return
	
	if username.count('.') > 1:
		errors['username'] = "Username can only have one period."

def _check_passwords(errors, pw1, pw2):
	if len(pw1) < 6:
		# "Password too short: 6 characters minimum."
		errors['password1'] = "Password specified is too short; it has to be at least 6 characters."
	elif pw1 != pw2:
		# "Passwords don't match."
		errors['password2'] = "Passwords don't match"

def render(req, tmpl, ctxt = None, status = 200):
	auth_service = req.app.auth_service
	tmpl = req.app.jinja_env.get_template(tmpl)
	if ctxt is None:
		ctxt = {}
	user = None
	ctxt['stats'] = req.app.stats
	ctxt['_role_to_str'] = _role_to_str
	logged_in = _verify_cookie(req)
	ctxt['logged_in'] = logged_in
	with Session() as sess:
		if logged_in:
			email = auth_service.get_token(PURPOSE_SITE, req.cookies[COOKIE_NAME])
			user = _get_user(email)
		if user is not None:
			ctxt['pending_groupchats'] = _get_groupchat_by_user(user)[1]
	content = tmpl.render(**ctxt)
	return web.Response(status = status, content_type = 'text/html', text = content)

PURPOSE_PWRESET = 'pwreset'
PURPOSE_SITE = 'site'
PURPOSE_REGISTRATION = 'register'

COOKIE_NAME = 'escargot'
REGISTRATION_COOKIE_NAME = 'newaccount'
